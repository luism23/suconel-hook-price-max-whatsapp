<?php
/*
Plugin Name: HOPMS WhatsApp Plugin
Plugin URI: https://gitlab.com/luism23/suconel-hook-price-max-whatsapp
Description: Este plugin oculta el botón de agregar al carrito en WooCommerce para productos con precio mayor a 500,000.00$ y agrega un botón de compra vía WhatsApp.
Version:1.0.1
Author: Startscoinc
Author URI: https://startscoinc.com/
*/

// Ocultar botón de agregar al carrito y agregar botón de compra vía WhatsApp

add_action('woocommerce_single_product_summary', 'HOPM_custom_single_product_summary', 1);
add_action('woocommerce_after_shop_loop_item', 'HOPM_custom_shop_loop_item', 1);

add_filter( 'body_class', 'add_class_to_body' );
function add_class_to_body( $classes ) {
    global $product;
    if ( is_product() && $product->get_price() > 500000 ) {
        $classes[] = 'product-price-max-500';
    }
    return $classes;
}


function HOPM_custom_shop_loop_item() {
    global $product;
  
    $price = $product->get_price();
  
    if ($price > 500000) {
      // Si el precio es mayor a 500000, ocultar el botón de agregar al carrito
      remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
  
      // Agregar el botón de "Comprar via WhatsApp"
      $product_name = $product->get_name();
      $product_price = $product->get_price();
      $whatsapp_message = "Me gustaría comprar este producto: $product_name, precio: $product_price";
      $whatsapp_message = strip_tags($whatsapp_message);
  
      if (wp_is_mobile() && wp_is_mobile('width<575')) {
        // Si es un dispositivo móvil y la pantalla es menor a 575px, utilizar el enlace de API de WhatsApp
        echo '<a href="https://api.whatsapp.com/send?phone=+573001539720&amp;text=' . urlencode($whatsapp_message) . '" class="button-whatsapp-shop alt hoptm-whatsapp-button hoptm-whatsapp-button-mobile"><span class="hoptm-whatsapp-icon"></span>Comprar via WhatsApp</a>';
      } else {
        // Si no, utilizar el enlace de WhatsApp Web
        echo '<a href="https://web.whatsapp.com/send?phone=+573001539720&amp;text=' . urlencode($whatsapp_message) . '" class="button-whatsapp-shop alt hoptm-whatsapp-button"><span class="hoptm-whatsapp-icon"></span>Comprar en WhatsApp</a>';
      }
    }
  }


add_action( 'woocommerce_after_shop_loop_item', 'add_class_to_shop_loop_item', 10 );
function add_class_to_shop_loop_item() {
    global $product;
    if ( $product->get_price() == 0 ) {
        global $post;
        $classes[] = 'product-value-0';
        $classes = array_filter( $classes );
        if ( ! empty( $classes ) ) {
            $class_output = 'class="' . esc_attr( implode( ' ', $classes ) ) . '"';
            echo '<div ' . $class_output . '></div>';
        }
    }
}

function add_bar_value_0_class_to_product_list_widget_li($classes, $product){
  if($product->get_price() == 0){
      $classes[] = 'bar-value-0';
  }
  return $classes;
}
add_filter('woocommerce_product_list_item_classes', 'add_bar_value_0_class_to_product_list_widget_li', 10, 2);